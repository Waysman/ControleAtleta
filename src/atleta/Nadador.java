package atleta;

public class Nadador extends Atleta {

	private String medalhas;
	private String recorde;
	
	
	
	public Nadador(String nome, char sexo) {
		super(nome, sexo);	
	}
	
	public String getMedalhas() {
		return medalhas;
	}
	public void setMedalhas(String medalhas) {
		this.medalhas = medalhas;
	}
	public String getRecorde() {
		return recorde;
	}
	public void setRecorde(String recorde) {
		this.recorde = recorde;
	}
	

	
}
