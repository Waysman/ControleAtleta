package atleta;

import java.util.ArrayList;
import java.util.Date;

public class Atleta {

	private ArrayList <String> telefones;
	private String nome;
	private Date dataNascimento;
	private Endereco endereco;
	private Double altura;
	private Double peso;
	
	private char sexo;
	private String rg;
	private String cpf;
	public Atleta(String nome, char sexo) {
		this.nome = nome;
		this.sexo = sexo;
		
		this.endereco = new Endereco();
	}
	public ArrayList<String> getTelefones() {
		return telefones;
	}
	public void setTelefones(ArrayList<String> telefones) {
		this.telefones = telefones;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public Double getAltura() {
		return altura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	
	
	
	
}
